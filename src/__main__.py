import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.sql import func

def incluir():
  print ('Informe os dados a seguir para incluir cliente')
  client = tb_cliente()
  client.nme_cliente = input("Nome: ")
  client.end_cliente = (input("Endereço: "))
  client.tel_cliente = (input("Telefone: "))
  ses.add(client)
  ses.commit()

def consultar():
  tb_clientes = ses.query(tb_cliente).all()
  print('Listagem com todos os clientes')
  for client in tb_clientes:
      print('-------------------------------------------------------------------')
      print('Nome:', client.nme_cliente)
      print('Endereço:', client.end_cliente, ' / Telefone:', client.tel_cliente)
  input("\nEnter - Para voltar ao Menu")

def alterar():
  # Descobrindo o menor e maior identificador cadastrado
  menor, maior = ses.query(func.min(tb_cliente.idt_cliente), func.max(tb_cliente.idt_cliente)).first()
  print('Alteração de dados de Cliente')
  idt = int(input('Alterar cliente número? [{}-{}]: '.format(menor, maior)))
  client = ses.query(tb_cliente).filter_by(idt_cliente=idt).first()
  client.nme_cliente = input('Nome [{}]: '.format(client.nme_cliente))
  client.end_cliente = (input('Endereço [{}]: '.format(client.end_cliente)))
  client.tel_cliente = (input('Telefone [{}]: '.format(client.tel_cliente)))
  ses.commit()
  input("\nEnter - Para voltar ao Menu")

def excluir():
  # Lista de clientes para apagar
  tb_clientes = ses.query(tb_cliente).order_by(tb_cliente.end_cliente.desc()).all()
  for client in tb_clientes:
      print(client.idt_cliente, client.nme_cliente, client.end_cliente)
  idt = int(input('Qual o número do cliente para excluir? '))
  ses.query(tb_cliente).filter(tb_cliente.idt_cliente == idt).delete()
  ses.commit()
  input("\nEnter - Para voltar ao Menu")

def menu():
  continuar = True
  while continuar:
      print('\n' * 30)
      print('CRUD de Clientes - MENU')
      print('1 - Incluir')
      print('2 - Consultar')
      print('3 - Alterar')
      print('4 - Excluir')
      print('5 - Sair')
      opc = input('Qual a sua opção? ')
      print("\n" * 30)
      if opc == '1':
          incluir()
      elif opc == '2':
          consultar()
      elif opc == '3':
          alterar()
      elif opc == '4':
          excluir()
      elif opc == '5':
          continuar = False
      else:
          print ('Opção inválida')
          input()
  # Finalizando o programa

  # Fechando a sessão
  ses.close()
  sys.exit()


# VARIÁVEIS GLOBAIS ------------------------------------
# Ligação com o esquema de banco de dados
engine = create_engine("mysql+mysqlconnector://root:hiragi7@db/db_work?charset=utf8mb4")

# Mapeamento Objeto Relacional com o SQLAlchemy
DB = automap_base()
DB.prepare(engine, reflect=True)
tb_cliente = DB.classes.tb_cliente

# Trabalho com sessões da base agora Objeto-Relacional
session_factory = sessionmaker(bind=engine)
ses = session_factory()
#-------------------------------------------------------------------------------------------------

# Iniciando o programa a partir do menu
menu()
